// @flow
import {FETCH_ERROR, FETCH_ADDED_USER, POST_USER} from "./addUserActions";
import type {User} from "../../ListUser/component/ListUser";

interface listUserStore {
  user: User;
  error: Object;
  isLoading: boolean;
}

const initialState: listUserStore = {
  isLoading: false,
  user: {username: '', name: '', email: ''},
  error: null
};

const addUserReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_USER:
      return {
        ...state,
        isLoading: true,
        data: action.payload
      };
    case FETCH_ADDED_USER:
      return {
        ...state,
        isLoading: false,
        user: action.payload
      };
    case FETCH_ERROR:
      return {
        ...state,
        isLoading: false
      };
    default:
      return {
        ...state,
        isLoading: false
      }
  }
};

export default addUserReducer;
