import {call, takeEvery, put} from 'redux-saga/effects';

import {FETCH_ADDED_USER, FETCH_ERROR, POST_USER} from "./addUserActions";
import {post} from "./api";

function* addUserSaga(action) {
  const url = 'https://json-fake-user-data.herokuapp.com/users';
  try{
    const user = yield call(post, url, action.payload);
    yield put({type: FETCH_ADDED_USER, payload: user})
  }
  catch(e) {
    yield put({type: FETCH_ERROR, payload: e})
  }
}

export  function* addUserWatcher() {
  yield takeEvery(POST_USER, addUserSaga)
}
