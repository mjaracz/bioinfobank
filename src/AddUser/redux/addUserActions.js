/* @flow */
export const POST_USER: string = 'POST_USER';
export const FETCH_ADDED_USER: string = 'FETCH_ADDED_USER';
export const FETCH_ERROR: string = 'FETCH_ERROR';

export const addUserAction = (data) => ({
  type: POST_USER,
  payload: data
});
