/* @flow */
import React, {p} from 'react';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

import '../style/AddUser.css';

interface Props {
  nicknameError: string;
  emailError: string;
  nameError: string;

  onChange(e: Event): void;
  onSubmit(): void;
}

const AddUserComponent = (props: Props) => {
  const {
    nicknameError,
    emailError,
    nameError,

    onChange,
    onSubmit
  } = props;
  return (
    <div className="addUser">
      <FormControl component={p}>
        <TextField
          name="nickname"
          label="Nickname"
          className="addUser__textField addUser__nickname"
          margin="normal"
          autoComplete="current-username"
          error={nicknameError.length !== 0}
          onChange={(e) => onChange(e)}
        />
        {
          nicknameError.length !== 0
            ? <FormHelperText style={{color: 'red'}}>{nicknameError}</FormHelperText>
            : null
        }
      </FormControl>
      <FormControl component={p}>
        <TextField
          name="name"
          label="Name"
          className="addUser__textField addUser__name"
          margin='normal'
          autoComplete="current-name"
          error={nameError.length !== 0}
          onChange={(e) => onChange(e)}
        />
        {
          nameError.length !== 0
            ? <FormHelperText style={{color: 'red'}}>{nameError}</FormHelperText>
            : null
        }
      </FormControl>
      <FormControl component={p}>
        <TextField
          name="email"
          label="Email"
          className="addUser__textField addUser__email"
          margin="normal"
          autoComplete="current-email"
          error={emailError.length !== 0}
          onChange={(e) => onChange(e)}
        />
        {
          emailError.length !== 0
            ? <FormHelperText style={{color: 'red'}}>{emailError}</FormHelperText>
            : null
        }
      </FormControl>
      <button
        className="addUser__button"
        onClick={onSubmit}
      >
        Add user
      </button>
    </div>
  )
};

export default AddUserComponent;
