/* @flow */
import React, {Component} from 'react';
import AddUser from '../component/AddUser';
import {addUserAction} from "../redux/addUserActions";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import type {User} from "../../ListUser/component/ListUser";

interface State {
  nicknameError: string;
  emailError: string;
  nameError: string;

  nickname: string;
  email: string;
  name: string;
}

interface Props {
  user: User;
  error: Object;
  isLoading: boolean;
  addUserAction(): Object;
}

class AddUserContainer extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      nicknameError: '',
      emailError: '',
      nameError: '',

      nickname: '',
      email: '',
      name: '',
    }
  }

  validateForm = () => {
    const {
      nickname,
      email,
      name
    } = this.state;
    const { addUserAction } = this.props;

    const isNotOnChangeStart = (nickname.length === 0 && name.length === 0 && email.length === 0);
    if (isNotOnChangeStart) return;

    const emailValidator = (email.includes('@'));
    const nameValidator = (name.length >= 5);
    const nicknameValidator = (nickname.length >= 3);
    const passValidations = emailValidator && nameValidator && nicknameValidator;

    this.setState({
      nameError: nameValidator ? '' : 'name is required, min of 5 char',
      nicknameError: nicknameValidator ? '' : 'nickname is required, min of 3 char',
      emailError: emailValidator ? '' : 'email is required'
    });

    if(passValidations) addUserAction({username: nickname, name, email});
  };
  onChange = (e: Event) => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    })
  };

  render() {
    const {
      nicknameError,
      emailError,
      nameError
    } = this.state;
    return (
      <AddUser
        nicknameError={nicknameError}
        emailError={emailError}
        nameError={nameError}

        onChange={this.onChange}
        onSubmit={this.validateForm}
      />
    )
  }
}

const mapStateToProps = state => ({
  user: state.addUser.user,
  error: state.addUser.error,
  isLoading: state.addUser.isLoading
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({
    addUserAction
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps) (AddUserContainer);
