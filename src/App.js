import React, {Fragment} from 'react';
import './App.css';
import ListUserContainer from './ListUser/container/ListUserContainer.jsx'
import AddUser from "./AddUser/container/AddUserContainer";
import HeaderComponent from './Header/Component/HeaderComponent';

function App() {
  return (
    <Fragment>
      <HeaderComponent/>
      <main className="main">
        <AddUser/>
        <ListUserContainer/>
      </main>
    </Fragment>
  );
}

export default App;
