import React from 'react';
import '../style/Header.css';

const HeaderComponent = () => {
  return (
    <header className="header">
      <h1 className="header__title">Crypto users</h1>
    </header>
  )
};

export default HeaderComponent;
