/* @flow */
import React from 'react';
import '../style/UserItemStyle.css';
import type {User} from "./ListUser";

interface Props {
  user: User
}

const UserItem = (props: Props) => {
  const {user} = props;
  return(
    <div className="main__userBox">
      <span className="userBox__item userBox__username">{user.username}</span>
      <span className="userBox__item userBox__name">{user.name}</span>
      <span className="userBox__item userBox__email">{user.email}</span>
    </div>
  )
};

export default UserItem;
