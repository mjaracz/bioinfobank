// @flow
import React, {Fragment} from 'react';
import UserItem from "./UserItem";
import '../style/ListUserStyle.css';

export type User = {
  id: number,
  name: string,
  username: string,
  email: string
}

interface Props {
  users: User[]
}

const ListUser = (props: Props) => {
  const {users} = props;
  const userItems = users.map((user, index)=>
    <UserItem
      user={user} 
      key={`${user.id}+${index}`}
    />
  );
  return (
    <Fragment>
      <div className="main__title">
        <span className="title__item title__username">Username</span>
        <span className="title__item title__name">Name</span>
        <span className="title__item title__email">Email</span>
      </div>
      {userItems}
    </Fragment>
  )
};

export default ListUser;
