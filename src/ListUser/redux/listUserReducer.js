import {FETCH_ERROR, FETCH_USER, GET_USERS} from "./listUserActions";
import type {User} from "../component/ListUser";

interface listUserStore {
  users: User[],
  error: Object,
  isLoading: boolean
}

const initialState: listUserStore = {
  users: [],
  error: null,
  isLoading: true
};

const listUserReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        isLoading: true
      };
    case FETCH_USER:
      return {
        ...state,
        users: action.payload,
        isLoading: false
      };
    case FETCH_ERROR:
      return {
        ...state,
        isLoading: true
      };
    default:
      return {
        ...state
      }
  }
};

export default listUserReducer;
