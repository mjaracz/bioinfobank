// @flow
export const GET_USERS: string = "GET_USER";
export const FETCH_USER: string = "FETCH_USER";
export const FETCH_ERROR: string = "FETCH_ERROR";


export const getUserAction = () => ({
  type: GET_USERS
});
