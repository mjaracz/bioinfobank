import {call, takeLatest, put} from 'redux-saga/effects';
import {FETCH_ERROR, FETCH_USER, GET_USERS} from "./listUserActions";
import {get} from './api'


function* fetchUser() {
  const url = 'https://json-fake-user-data.herokuapp.com/users';
  try {
    const users = yield call(get, url);
    yield put({type: FETCH_USER, payload: users})
  }
  catch (e) {
    yield put({type: FETCH_ERROR, payload: e});
    throw new Error(e);
  }
}

export function* userListWatcher() {
  yield takeLatest(GET_USERS, fetchUser)
}
