/* @flow */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {getUserAction} from "../redux/listUserActions";

import ListUser from '../component/ListUser';
import {CircularProgress} from "@material-ui/core";
import type {User} from "../component/ListUser";

interface Props {
  users: User[],
  error: Object,
  isLoading: boolean,
  getUserAction(): Object
}

class ListUserContainer extends Component<Props> {
  async componentDidMount() {
    await this.props.getUserAction();
  }
  render() {
    const {
      isLoading,
      error,
      users,

      addedUser,
      isLoadingADD_USER
    } = this.props;
    if(addedUser.id) users.push(addedUser);
    return (
      <div className='main--userList'>
        {
          (isLoading || isLoadingADD_USER)
            ? <CircularProgress size={120}/>
            : <ListUser
              users={users}
            />
        }
        {
          error
            ? console.error(error.message)
            : null
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  users: state.listUsers.users,
  isLoading: state.listUsers.isLoading,
  error: state.listUsers.error,

  addedUser: state.addUser.user,
  isLoadingADD_USER: state.addUser.isLoading
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    getUserAction
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ListUserContainer);
