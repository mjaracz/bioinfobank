import { combineReducers } from "redux";
import listUserReducer from '../ListUser/redux/listUserReducer'
import addUserReducer from "../AddUser/redux/addUserReducer";

const rootReducer = combineReducers({
  listUsers: listUserReducer,
  addUser: addUserReducer
});


export default rootReducer;
