import {all} from 'redux-saga/effects';
import {userListWatcher} from "../ListUser/redux/listUserSaga";
import {addUserWatcher} from "../AddUser/redux/addUserSaga";

export function* rootSaga() {
  yield all([
    userListWatcher(),
    addUserWatcher()
  ]);
}
