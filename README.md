Environmental requirements:
> npm:  ^6.9.0 <br>
> NodeJS:  ^10.16.0 <br>
> git: ^2.17.0 <br>

First of all, in your favorite terminal, go to the directory, where want to keep whole project. <br> <br>
Then start typing with, super administrator privilege : <br>
> git clone https://www.gitlab.com/mjaracz/bioinfobank; <br>

In the next command, type:

> npm install;

And the last one witch start whole project:

> npm start;

That it, after this step..

Good luck and happy hacking :)
